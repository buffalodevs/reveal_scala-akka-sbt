# An introduction to Scala, Akka, and SBT

A Reveal.js presentation for #BuffaloDevs / #buffalo-scala

## Latest Build
This page is built/hosted via GitLab pages and can be viewed at:
[https://buffalodevs.gitlab.io/reveal_scala-akka-sbt/](https://buffalodevs.gitlab.io/reveal_scala-akka-sbt/)
